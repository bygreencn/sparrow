#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/12/7'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

import os

from models import states
from models.initialize import app

app.config['PROCESS_TITLE'] = 'sparrow'
app.config['DEBUG'] = True
# d for Day, w for Week, m for month
app.config['DUMP_CYCLE'] = 'w'
app.config['DATA_FLY_TIME'] = 3600
app.config['DATABASE'] = 'sparrow'
app.config['DB_PORT'] = 3306
app.config['DB_RESULT_LIMIT'] = 1000
# 冷热库的时间维度字段名,时间范围值,将参考该字段
app.config['TIME_LINE_FIELD'] = 'create_time'
app.config['LOG_FILE_BASE'] = ''.join([os.getcwd(), '/logs/log'])
app.config['IDsKeeper'] = 'H:IDsKeeper'
app.config['REDIS'] = {
    'host': '127.0.0.1',
    'port': 2301,
    'db': 0,
    'password': ''
}

app.config['DB_S'] = [
    {
        'domain': states.DBDomain.light.value,
        'rw_mode': states.RWMode.SW_SR.value,
        # 第一组为写,第二组为读.之后的无效
        'db_group_s': [
            [
                {
                    'host': '127.0.0.1',
                    'user': 'root',
                    'password': 'dbpswd',
                    # Max 32
                    'pool_size': 5
                }
            ],
            [
                {
                    'host': '127.0.0.1',
                    'user': 'root',
                    'password': 'dbpswd',
                    'pool_size': 5
                }
            ]
        ]
    },
    {
        'domain': states.DBDomain.hot.value,
        'rw_mode': states.RWMode.BW_AR.value,
        # 第一组为写,之后的每组都为读,最多三组读
        'db_group_s': [
            [
                {
                    'host': '127.0.0.1',
                    'user': 'root',
                    'password': 'dbpswd',
                    'database': 'sparrow__hot',
                    'pool_size': 5
                }
            ],
            [
                {
                    'host': '127.0.0.1',
                    'user': 'root',
                    'password': 'dbpswd',
                    'database': 'sparrow__hot',
                    'pool_size': 5
                }
            ],
            [
                {
                    'host': '127.0.0.1',
                    'user': 'root',
                    'password': 'dbpswd',
                    'database': 'sparrow__hot',
                    'pool_size': 5
                }
            ]
        ]
    },
    {
        'domain': states.DBDomain.cold.value,
        'rw_mode': states.RWMode.SW_SR.value,
        # 第一组为写,第二组为读.之后的无效
        'db_group_s': [
            [
                {
                    'host': '127.0.0.1',
                    'user': 'root',
                    'password': 'dbpswd',
                    'pool_size': 5,
                    # 大周期标记
                    'sequence_flag': 2016
                }
            ],
            [
                {
                    'host': '127.0.0.1',
                    'user': 'root',
                    'password': 'dbpswd',
                    'pool_size': 5,
                    'sequence_flag': 2014
                },
                {
                    'host': '127.0.0.1',
                    'user': 'root',
                    'password': 'dbpswd',
                    'pool_size': 5,
                    'sequence_flag': 2015
                },
                {
                    'host': '127.0.0.1',
                    'user': 'root',
                    'password': 'dbpswd',
                    'pool_size': 5,
                    'sequence_flag': 2016
                }
            ]
        ]
    }
]

