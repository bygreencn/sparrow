#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/12/7'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

import traceback
import thread
from models.initialize import app, Init, logger, objects_model, object_db_map, db_conn_map, cold_db_conn_map,\
    cold_db_conn_map_list, cold_db_conn_map_list_reversed, r
from views.recode_operate import blueprint as recode_operate_blueprint
import route_table


if __name__ == '__main__':
    # noinspection PyBroadException
    try:
        Init.init_db_conn()
        Init.init_cold_db_conn()
        Init.init_object_db_map()
        Init.init_objects_model()
        app.register_blueprint(recode_operate_blueprint)
        thread.start_new_thread(Init.db_keepalived, ())

        app.run(host='0.0.0.0', debug=app.config['DEBUG'], use_reloader=False, threaded=True)
    except:
        logger.error(traceback.format_exc())
