#!/usr/bin/env python
# -*- coding: utf-8 -*-

import jimit as ji
from enum import IntEnum

__author__ = 'James Iter'
__date__ = '15/12/7'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'


class DBDomain(IntEnum):
    light = 0
    hot = 1
    cold = 2

    @staticmethod
    def get_label(domain):
        from models.rules import Rules
        args_rules = [
            Rules.DOMAIN.value,
        ]
        ji.Check.previewing(args_rules, locals())

        label = None
        if domain == DBDomain.light.value:
            # label = 'light'
            label = u'轻库'
        elif domain == DBDomain.hot.value:
            # label = 'hot'
            label = u'热库'
        elif domain == DBDomain.cold.value:
            # label = 'cold'
            label = u'冷库'
        else:
            raise TypeError('unknown domain')

        return label


class RWMode(IntEnum):
    # BW for balance write, AR for aggregation read
    BW_AR = 1
    # SW for singleton write, AR for aggregation read
    SW_SR = 2

    @staticmethod
    def get_label(rw_mode):
        from models.rules import Rules
        args_rules = [
            Rules.RW_MODE.value,
        ]
        ji.Check.previewing(args_rules, locals())

        label = None
        if rw_mode == RWMode.BW_AR.value:
            # label = 'balance write'
            label = u'均衡写, 聚合读'
        elif rw_mode == RWMode.SW_SR.value:
            # label = 'singleton write'
            label = u'单例写, 单例读'
        else:
            raise TypeError('unknown rw_mode')

        return label
