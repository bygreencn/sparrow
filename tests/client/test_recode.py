#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/12/20'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'


import unittest
import requests
import json


class TestRecode(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    # ------------------------------------------------注册---------------------------------------------------------
    # 用户注册
    # def test_10_post(self):
    #     url = 'http://ezs.ez-iso.com:5000/oo/login_auth'
    #     r = requests.post(url=url, json={'login_name': 'fanliang@iqusong.com',
    #                                      'password': '000000.com',
    #                                      'status': 1,
    #                                      'roles': 1,
    #                                      'create_time': 0,
    #                                      'salt': '1',
    #                                      'id': 10005})
    #     print r.text.encode('utf-8')
    #     self.assertEqual('200', json.loads(r.text)['state']['code'])

    # def test_20_patch(self):
    #     url = 'http://ezs.ez-iso.com:5000/oo/login_auth/10005'
    #     r = requests.patch(url=url, json={'login_name': 'jimit@qq.com',
    #                                       'password': '111111.com'})
    #     print r.text.encode('utf-8')
    #     self.assertEqual('200', json.loads(r.text)['state']['code'])

    def test_30_delete(self):
        url = 'http://ezs.ez-iso.com:5000/oo/login_auth/10005'
        r = requests.delete(url=url)
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])
