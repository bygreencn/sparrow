CREATE DATABASE IF NOT EXISTS sparrow;
USE sparrow;

CREATE TABLE IF NOT EXISTS login_auth(
    id BIGINT UNSIGNED NOT NULL,
    login_name VARCHAR(30) NOT NULL,
    password VARCHAR(100) NOT NULL,
    create_time BIGINT NOT NULL,
    PRIMARY KEY (id))
    ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS user_info(
    id BIGINT UNSIGNED NOT NULL,
    first_name VARCHAR(30) NOT NULL,
    last_name VARCHAR(30) NOT NULL,
    nick_name VARCHAR(30) NOT NULL,
    salary BIGINT NOT NULL,
    PRIMARY KEY (id))
    ENGINE=InnoDB;

ALTER TABLE login_auth ADD INDEX (login_name);
ALTER TABLE login_auth ADD INDEX (create_time);
ALTER TABLE user_info ADD INDEX first_name__last_name (first_name, last_name);


CREATE DATABASE IF NOT EXISTS sparrow__hot;
USE sparrow__hot;
CREATE TABLE IF NOT EXISTS order_form(
  id BIGINT UNSIGNED NOT NULL,
  create_time BIGINT NOT NULL,
  finished_time BIGINT NOT NULL,
  PRIMARY KEY (id))
  ENGINE=InnoDB;
ALTER TABLE order_form ADD INDEX (create_time);

CREATE DATABASE IF NOT EXISTS sparrow__2015__10;
USE sparrow__2015__10;
CREATE TABLE IF NOT EXISTS order_form(
  id BIGINT UNSIGNED NOT NULL,
  create_time BIGINT NOT NULL,
  finished_time BIGINT NOT NULL,
  PRIMARY KEY (id))
  ENGINE=InnoDB;
ALTER TABLE order_form ADD INDEX (create_time);

CREATE DATABASE IF NOT EXISTS sparrow__2015__11;
USE sparrow__2015__11;
CREATE TABLE IF NOT EXISTS order_form(
  id BIGINT UNSIGNED NOT NULL,
  create_time BIGINT NOT NULL,
  finished_time BIGINT NOT NULL,
  PRIMARY KEY (id))
  ENGINE=InnoDB;
ALTER TABLE order_form ADD INDEX (create_time);


CREATE DATABASE IF NOT EXISTS sparrow__2015__12;
USE sparrow__2015__12;
CREATE TABLE IF NOT EXISTS order_form(
    id BIGINT UNSIGNED NOT NULL,
    create_time BIGINT NOT NULL,
    finished_time BIGINT NOT NULL,
    PRIMARY KEY (id))
    ENGINE=InnoDB;
ALTER TABLE order_form ADD INDEX (create_time);


CREATE DATABASE IF NOT EXISTS sparrow__2016__1;
USE sparrow__2016__1;
CREATE TABLE IF NOT EXISTS order_form(
    id BIGINT UNSIGNED NOT NULL,
    create_time BIGINT NOT NULL,
    finished_time BIGINT NOT NULL,
    PRIMARY KEY (id))
    ENGINE=InnoDB;
ALTER TABLE order_form ADD INDEX (create_time);


/*
DROP DATABASE sparrow;
DROP DATABASE sparrow__hot;
DROP DATABASE sparrow__2015__10;
DROP DATABASE sparrow__2015__11;
DROP DATABASE sparrow__2015__12;
DROP DATABASE sparrow__2016__1;
*/
