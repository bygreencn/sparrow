#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '16/1/12'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'


import zmq


# Prepare our context and sockets
context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect('tcp://localhost:3000')


message = dict()


message['action'] = 'PATCH'
message['object_name'] = 'order_form'
message['parms'] = {
    'id': 1,
    'create_time': 100
}
socket.send_json(message)
socket.recv_json()

