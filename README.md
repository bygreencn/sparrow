# Sparrow
分布式数据库中间件

## 安装
``` python
pip install -r requirements.txt -i http://pypi.douban.com/simple --trusted-host pypi.douban.com --allow-external mysql-connector-python
```

## 启动
``` python
python sparrow.py
```

## HTTP API
### 插入
``` shell
curl -X "POST" "http://localhost:5000/oo/login_auth" \
	-H "Content-Type: application/json" \
	-d "{\"id\":1,\"login_name\":\"fanliang@iqusong.com\",\"password\":\"000000.com\",\"create_time\":0}"
```

### 更新
``` shell
curl -X "PATCH" "http://localhost:5000/oo/login_auth/1" \
	-H "Content-Type: application/json" \
	-d "{\"password\":\"newpswd\"}"
```

### 获取
``` shell
curl -X "GET" "http://localhost:5000/oo/order_form?filter_str=__order_by__id,__limit__1000,__range__1451197800~1451518200"
```

### 删除
``` shell
curl -X "DELETE" "http://localhost:5000/oo/login_auth/1"
```

## zmq API
### RPC
``` python
    message = dict()
    message['action'] = 'RPC'
    message['object_name'] = 'order_form'
    message['parms'] = {
        'function': 'generate_id_by',       # 支持的方法 'generate_id_by | get_id_of_max_by'
    }
    socket.send_json(message)
    result = socket.recv_json()
    
    
    result:
    {
        state: {
            code: "200",
            zh-cn: "成功",
            en-us: "OK"
        }
        id: 1
    }
```

### 插入
``` python
message = dict()
message['action'] = 'POST'
message['object_name'] = 'order_form'
message['parms'] = {
    'id': 1,
    'create_time': 0,
    'finished_time': 0
}
socket.send_json(message)
socket.recv_json()

result:
{
    state: {
        code: "200",
        zh-cn: "成功",
        en-us: "OK"
    }
}
```

### 更新
``` python
message = dict()
message['action'] = 'PATCH'
message['object_name'] = 'order_form'
message['parms'] = {
    'id': 1,
    'create_time': 0,
    'finished_time': 0
}
socket.send_json(message)
socket.recv_json()

result:
{
    state: {
        code: "200",
        zh-cn: "成功",
        en-us: "OK"
    }
}
```

### 获取
``` python
message = dict()
message['action'] = 'GET'
message['object_name'] = 'order_form'
message['parms'] = {
    'filter_str': '__order_by__id,__limit__1000,__range__1451197800~1451518200'
}
socket.send_json(message)
socket.recv_json()

result:
{
    state: {
        code: "200",
        zh-cn: "成功",
        en-us: "OK"
    },
    list: [
        {}
    ]
}
```

### 删除
``` python
message = dict()
message['action'] = 'DELETE'
message['object_name'] = 'order_form'
message['parms'] = {
    'id': 1
}
socket.send_json(message)
socket.recv_json()

result:
{
    state: {
        code: "200",
        zh-cn: "成功",
        en-us: "OK"
    }
}
```

## filter_str语法
### __eq__
``` shell
id__eq__100
id等于100的记录
```

### __gt__
``` shell
id__gt__100
id大于100的记录
```

### __lt__
``` shell
id__lt__100
id小于100的记录
```

### __ne__
``` shell
id__ne__100
id不等于100的记录
```

### __in__
``` shell
id_in__100~101~103
id等于100或101或103的记录
```

### __order_by__
``` shell
__order_by__create_time
依据create_time字段顺序排序
```

### __order_by_desc__
``` shell
__order_by_desc__create_time
依据create_time字段倒序排序
```

### __limit__
``` shell
__limit__10
限制返回前10条记录
```

### __range__
``` shell
__range__1451197800~1451518200
以1451197800~1451518200时间范围限定查找空间
```

### 综合示例
``` shell
__order_by_desc__id,__limit__1000,__range__1451197800~1451518200
依据id字段,倒序返回1451197800~1451518200时间空间中的前1000条记录

id__gt__100,id__lt__1000,name__eq__james
返回id大于100且小于1000的记录中,name等于james的记录
```

## 错误码
``` python
    error_codes = {
        '41250': {
            'code': '41250',
            'zh-cn': u'未支持索引的字段'
        },
        '50050': {
            'code': '50150',
            'zh-cn': u'MySQL 链接或执行出错'
        },
        '50051': {
            'code': '50051',
            'zh-cn': u'Redis 链接或执行出错'
        }
    }
```